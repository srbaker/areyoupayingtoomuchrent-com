# Are you paying too much rent?

On Prince Edward Island, rent increases are regulated by the
government, and are _tied to the property not the tenant_.  Because of
this, it is possible that you are paying too much rent.  If you are
paying too much rent, your landlord will be required to lower your
rent to the allowable limit, _and refund you for any overages you have
paid to date_.

If you know the rent that was paid on the property in any year since
1989, you can use this tool to find out what the maximum rent for your
property is.  Since the rent *follows the property*, this applies even
if you learn what a previous tenant was paying!

You may feel that you are paying a "fair" amount of rent for your
property.  If you are paying more than the legally allowed amount,
that is not fair, according to the law.  Your landlord is likely
extorting you and extortion is bad.

If you use this tool and find that you are paying too much rent,
please contact IRAC _immediately_ and inform them so that they can
make sure that your landlord is following the law.  And get your money
back in your pocket where it belongs.


## The Software

This software is presented as a single webpage, using a small amount
of JavaScript.  It should work in any browser.  If it doesn't work in
your browser, let me know, or submit a patch!

This software is released under the GNU Affero General Public License
version 3, or at your option, any later version.  You can find a copy
of the AGPL in the COPYING file.


## Embedding the Calculator

The calculator is made for embedding in your own website!  To do so,
you need a form with two fields: year, and amount.  And an element
that should be updated with the maximum allowed rent.

You then call the function `updateMaximumAllowedRent(formName,
yearName, amountName, id)`.  You pass the name of the form `formName`,
the names of the year and amount fields `yearName`, and `amountName`,
and the `id` of the element whose text content should be updated with
the maximum allowed rent.

See `index.html` for the example of this in action.


## Contributions

Contributions are not only welcome, they're encouraged and
appreciated!

Keep in mind that the size and design of the software has been
carefully considered and is deliberate.  We will _not_ use your
favourite "framework", and external dependencies (except small things
that assist in automated testing) will only be considered with the
most extreme caution.

I would love to add support for jurisdictions outside of PEI: if you
can point me in the direction of rent control rules for other
jurisdictions I will do my best to implement them myself.  If you
contribute the additions, I will be happy to integrate them!
