/* This file is part of areyoupayingtoomuchrent.com.

   areyoupayingtoomuchrent.com is free software: you can redistribute
   it and/or modify it under the terms of the GNU Affero General
   Public License as published by the Free Software Foundation, either
   version 3 of the License, or (at your option) any later version.

   areyoupayingtoomuchrent.com is distributed in the hope that it will
   be useful, but WITHOUT ANY WARRANTY; without even the implied
   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public
   License along with areyoupayingtoomuchrent.com.  If not, see
   <https://www.gnu.org/licenses/>. */
const rentIncreaseAllowances = {
    /* FIXME: put the percentage as a string so it's easier to visually verify */
    2021: { furnaceOilHeat: 0.0100, electricHeat: 0.0100, unheated: 0.0100, mobileHome: 0.0100 },
    2020: { furnaceOilHeat: 0.0130, electricHeat: 0.0130, unheated: 0.0130, mobileHome: 0.0130 },
    2019: { furnaceOilHeat: 0.0200, electricHeat: 0.0175, unheated: 0.0150, mobileHome: 0.0150 },
    2018: { furnaceOilHeat: 0.0175, electricHeat: 0.0175, unheated: 0.0150, mobileHome: 0.0150 },
    2017: { furnaceOilHeat: 0.0150, electricHeat: 0.0150, unheated: 0.0150, mobileHome: 0.0150 },
    2016: { furnaceOilHeat: 0.0000, electricHeat: 0.0000, unheated: 0.0000, mobileHome: 0.0000 },
    2015: { furnaceOilHeat: 0.0175, electricHeat: 0.0175, unheated: 0.0100, mobileHome: 0.0100 },
    2014: { furnaceOilHeat: 0.0200, electricHeat: 0.0200, unheated: 0.0100, mobileHome: 0.0100 },
    2013: { furnaceOilHeat: 0.0500, electricHeat: 0.0500, unheated: 0.0300, mobileHome: 0.0150 },
    2012: { furnaceOilHeat: 0.0320, electricHeat: 0.0320, unheated: 0.0200, mobileHome: 0.0100 },
    2011: { furnaceOilHeat: 0.0200, electricHeat: 0.0200, unheated: 0.0100, mobileHome: 0.0100 },
    2010: { furnaceOilHeat: 0.0200, electricHeat: 0.0200, unheated: 0.0200, mobileHome: 0.0200 },
    2009: { furnaceOilHeat: 0.0500, electricHeat: 0.0500, unheated: 0.0300, mobileHome: 0.0300 },
    2008: { furnaceOilHeat: 0.0100, electricHeat: 0.0100, unheated: 0.0100, mobileHome: 0.0000 },
    2007: { furnaceOilHeat: 0.0300, electricHeat: 0.0300, unheated: 0.0300, mobileHome: 0.0100 },
    2006: { furnaceOilHeat: 0.0750, electricHeat: 0.0750, unheated: 0.0350, mobileHome: 0.0350 },
    2005: { furnaceOilHeat: 0.0200, electricHeat: 0.0200, unheated: 0.0200, mobileHome: 0.0200 },
    2004: { furnaceOilHeat: 0.0200, electricHeat: 0.0200, unheated: 0.0200, mobileHome: 0.0200 },
    2003: { furnaceOilHeat: 0.0300, electricHeat: 0.0300, unheated: 0.0300, mobileHome: 0.0300 },
    2002: { furnaceOilHeat: 0.0225, electricHeat: 0.0225, unheated: 0.0225, mobileHome: 0.0225 },
    2001: { furnaceOilHeat: 0.0375, electricHeat: 0.0375, unheated: 0.0125, mobileHome: 0.0125 },
    2000: { furnaceOilHeat: 0.0150, electricHeat: 0.0150, unheated: 0.0150, mobileHome: 0.0150 },
    1999: { furnaceOilHeat: 0.0100, electricHeat: 0.0100, unheated: 0.0100, mobileHome: 0.0100 },
    1998: { furnaceOilHeat: 0.0150, electricHeat: 0.0150, unheated: 0.0150, mobileHome: 0.0150 },
    1997: { furnaceOilHeat: 0.0150, electricHeat: 0.0150, unheated: 0.0150, mobileHome: 0.0150 },
    1996: { furnaceOilHeat: 0.0200, electricHeat: 0.0200, unheated: 0.0200, mobileHome: 0.0200 },
    1995: { furnaceOilHeat: 0.0000, electricHeat: 0.0000, unheated: 0.0000, mobileHome: 0.0000 },
    1994: { furnaceOilHeat: 0.0250, electricHeat: 0.0250, unheated: 0.0250, mobileHome: 0.0250 },
    1993: { furnaceOilHeat: 0.0100, electricHeat: 0.0100, unheated: 0.0100, mobileHome: 0.0100 },
    1992: { furnaceOilHeat: 0.0300, electricHeat: 0.0300, unheated: 0.0300, mobileHome: 0.0300 },
    1991: { furnaceOilHeat: 0.0450, electricHeat: 0.0450, unheated: 0.0450, mobileHome: 0.0450 },
    1990: { furnaceOilHeat: 0.0400, electricHeat: 0.0400, unheated: 0.0400, mobileHome: 0.0400 },
    1989: { furnaceOilHeat: 0.0400, electricHeat: 0.0400, unheated: 0.0400, mobileHome: 0.0400 },        
}

function allowedIncreasesSince(year, propertyKind="furnaceOilHeat") {
    const currentYear = new Date().getFullYear();
    const increases = Array((currentYear - year) + 1).fill(1).map((_, idx) => currentYear - idx);

    return increases.map(year => rentIncreaseAllowances[year][propertyKind]);
}

function maximumAllowedRent(year, rent) {
    const reducer = (rent, increase) => rent * (1 + increase);
    const maxRent = allowedIncreasesSince(year).reduce(reducer, rent);

    return Math.round(maxRent * 100) / 100;
}

function updateMaximumAllowedRent(formName, yearName, amountName, id) {
    const year = parseInt(document.forms[formName][yearName].value);
    const rent = parseFloat(document.forms[formName][amountName].value);

    document.getElementById(id).textContent = maximumAllowedRent(year, rent);
}
